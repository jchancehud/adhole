const dgram = require('dgram')

const upstreamAddress = '208.67.222.222'

module.exports = {
  sliceBits,
  qnameToDomain,
  proxyDNS,
}

// Slice single byte into bits
// only pass a single byte
function sliceBits(_byte, _offset, _length) {
  const s = 7 - (_offset + _length - 1)
  let __byte = _byte
  __byte = __byte >>> s
  return __byte & ~(0xff << _length)
}

function qnameToDomain(qname) {
  let domain = ''
  /**
    qname:
    a domain name represented as a sequence of labels, where
    each label consists of a length octet followed by that
    number of octets.  The domain name terminates with the
    zero length octet for the null label of the root.
  **/
  for (let i = 0; i < qname.length; i++) {
    if (qname[i] == 0) {
      domain = domain.substring(0, domain.length - 1)
      break
    }
    let tmpBuf = qname.slice(i + 1, i + qname[i] + 1)
    domain += tmpBuf.toString('binary', 0, tmpBuf.length)
    domain += '.'
    i = i + qname[i]
  }
  return domain
}

// Execute lookups in serial queue creating and destroying udp socket on single
// port
let promiseQueue = Promise.resolve()
function proxyDNS(msg, rinfo, server) {
  promiseQueue = promiseQueue.then(() => {
    let rs, rj, resolved
    const p = new Promise((_rs, _rj) => {
      rs = _rs
      rj = _rj
    })
      .then(() => { resolved = true })
      .catch((e) => {
        resolved = true
        throw e
      })
    const client = dgram.createSocket('udp4')
    client.bind(1338, '0.0.0.0')
    client.on('message', (r, ri) => {
      // console.log('received upstream response')
      server.send(r, 0, ri.size, rinfo.port, rinfo.address)
      client.close()
      rs()
    })
    client.on('error', (e) => {
      console.log('upstream error', e)
      client.close()
      if (e) rj(e)
    })
    client.send(msg, 0, msg.length, 53, upstreamAddress, (e) => {
      if (e) {
        client.close()
        rj(e)
      }
    })
    setTimeout(() => {
      if (!resolved) {
        try {
          client.close()
        } catch (err) {}
        rj(new Error('Upstream timeout'))
      }
    }, 5000)
    return p
  })
  return promiseQueue
}
