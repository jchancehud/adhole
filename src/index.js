const dns = require('dns')
const dgram = require('dgram')
const { proxyDNS, sliceBits, qnameToDomain } = require('./resolver')
const { loadBlocklists } = require('./blocklists')

let blockedDomains = {}
setInterval(async () => {
  try {
    blockedDomains = await loadBlocklists()
  } catch (err) {
    console.log(err)
    console.log('Error reloading blocklists!')
  }
}, 1000 * 60 * 60 * 6)
loadBlocklists()
  .then((_blockedDomains) => { blockedDomains = _blockedDomains })
  .catch((err) => {
    console.log(err)
    console.log('Error loading blocklists')
    process.exit(1)
  })

// https://nodejs.org/api/dgram.html
const server = dgram.createSocket('udp4')

server.on('error', (err) => {
  console.log('Server error', err)
  console.log(err.stack)
  process.exit(1)
})

server.on('message', async (msg, rinfo) => {
  // console.log('server message received')
  //console.log(`Server message: ${msg.toString('binary')}`)
  const bMessage = msg.slice(0, 1)
  const txId = msg.slice(0, 2)
  let _byte = msg.slice(2, 3).toString('binary', 0, 1).charCodeAt(0)
  // Query or response
  const qr = sliceBits(_byte, 0, 1)
  // Query type
  const opcode = sliceBits(_byte, 1, 4)
  // Authoritative answer (response only)
  const aa = sliceBits(_byte, 5, 1)
  // Truncated?
  const tc = sliceBits(_byte, 6, 1)
  // Recursion desired?
  const rd = sliceBits(_byte, 7, 1)
  const qdcount = msg.slice(4, 6).readUInt16BE()
  // Only parse enough to do a blocklist comparison
  const qStart = 12
  const question = msg.slice(qStart, msg.length - 4)
  const domain = qnameToDomain(question)
  // console.log('qdcount', qdcount)
  // If domain is blocklisted send an altered request
  // otherwise forward to upstream dns server
  if (blockedDomains[domain.toLowerCase()]) return console.log('blocking domain', domain)
  try {
    await proxyDNS(msg, rinfo, server)
  } catch (e) {
    console.log('Error proxying dns', e)
  }
})

server.on('listening', () => {
  const { address, port } = server.address()
  console.log(`UDP server listening on port ${address}:${port}`)
})

server.bind(1337, '0.0.0.0', () => {
  console.log('Server bound')
})
