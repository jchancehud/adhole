const fs = require('fs')
const path = require('path')
const axios = require('axios')

module.exports = {
  loadBlocklists,
}

const blocklistUrls = [
  'https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts',
  'https://mirror1.malwaredomains.com/files/justdomains',
  'https://s3.amazonaws.com/lists.disconnect.me/simple_tracking.txt',
  'https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt',
  'https://raw.githubusercontent.com/kboghdady/youTube_ads_4_pi-hole/master/youtubelist.txt',
  'https://raw.githubusercontent.com/justdomains/blocklists/master/lists/adguarddns-justdomains.txt',
  'https://raw.githubusercontent.com/justdomains/blocklists/master/lists/easylist-justdomains.txt',
  'https://raw.githubusercontent.com/justdomains/blocklists/master/lists/easyprivacy-justdomains.txt',
  'https://raw.githubusercontent.com/justdomains/blocklists/master/lists/nocoin-justdomains.txt',
  'https://raw.githubusercontent.com/notracking/hosts-blocklists/master/dnscrypt-proxy/dnscrypt-proxy.blacklist.txt',
  'https://dbl.oisd.nl',
]

console.log('Loading blocklists...')
async function loadBlocklists() {
  const localBlocklist = fs.readFileSync(path.join(__dirname, '..', 'blocklist.txt'))
  const blockedDomains = {}
  for (const _url of blocklistUrls) {
    let lines
    try {
      const { data } = await axios(_url)
      Object.assign(blockedDomains, parseBlocklist(data))
    } catch (err) {
      console.log('Failed to load blocklist at url')
      console.log(_url)
      continue
    }
  }
  console.log(`Loaded domain blocklists (${Object.keys(blockedDomains).length} domains).`)
  return blockedDomains
}

function parseBlocklist(data) {
  const blockedDomains = {}
  const lines = data.split('\n')
  for (const line of lines) {
    if (line.indexOf('#') !== -1) continue
    const blocks = line.trim().split(' ')
    if (blocks.length === 0) continue
    const address = blocks[blocks.length - 1]
    blockedDomains[address.toLowerCase()] = true
  }
  return blockedDomains
}
