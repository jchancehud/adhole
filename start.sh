#!/bin/sh

set -e

NAME=adhole

ID=$(docker ps -f name=$NAME -q)
if [ ! -z $ID ]
then
  docker stop $NAME
fi

docker run -d --restart always --name $NAME -p 1338:1338/udp -p 53:1337/udp $(docker build . -q)
exec docker logs -f $NAME
