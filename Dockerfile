FROM alpine:latest
MAINTAINER Chance Hudson

RUN apk add --no-cache git make bash nodejs-npm

COPY . /src
WORKDIR /src

RUN npm install

CMD ["npm", "start"]
